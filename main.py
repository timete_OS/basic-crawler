# from uuid import uuid4
import requests
import re
import time

import numpy as np

from urllib.parse import urlparse
from datetime import datetime
from lxml import html
from lxml import etree, objectify

from threading import Event, Thread

import queue


from dotenv import load_dotenv
from unidecode import unidecode

# from dataclasses import dataclass


# load_dotenv()



line = queue.Queue(maxsize=1000)
event = Event()



url_to_time = {}
final_urls = []
fh = open('crawled_webpages.txt', 'r+')

# thanks stackoverflow
regex = re.compile(
    r'^https?://'  # http:// or https://
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|'  # domain...
    r'localhost|'  # localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
    r'(?::\d+)?'  # optional port
    r'(?:/?|[/?]\S+)$', re.IGNORECASE)


def is_valid_url(url):
    return url is not None and regex.search(url)

# thread safe ?
def politeness(url:str):
    global url_to_time
    url = urlparse(url).hostname
    res = url_to_time.get(url)
    if res:
        delta = datetime.now() - res
        if delta.seconds < 5:
            time.sleep(5 - delta.seconds)
    url_to_time[url] = datetime.now()


def read_sitemap(url:str):
    # useless for now

    # res = requests.get(url + 'sitemap.xml')
    # tree = etree.fromstring(res.content)

    # # for l in root:
    # #     print(f"{l[0].text}, {l[1].text}")

    # pretty_xml = etree.tostring(tree, pretty_print=True, encoding=str)
    # # print(pretty_xml)

    # urls = tree.findall('.//url')
    # for url in urls[:3]:
    #     print(url)

    return None


def add_urls(urls:list):
    global final_urls
    final_urls += urls
    for url in urls:
        fh.write(url)
        fh.write('\n')

def crawl(urls:list):
    urls = set(urls)
    external_links = set()
    for url in urls:
        page = requests.get(url)
        tree = html.fromstring(page.content)
        links = tree.xpath('//a/@href')
        links = [link for link in links if is_valid_url(link)]
        external_links = external_links.union(set(links))
    return external_links.difference(urls)


def consume():
    print("Thread Start !")
    while line.qsize():
        # print("UN PASSAGE")
        url = line.get()

        if event.is_set():
            return

        # be polite, wait for your turn
        politeness(url)

        # check for robots.txt
        robot = requests.get(url + "/robots.txt")
        
        # check for sitemap
        urls = read_sitemap(url)

        if urls is None:
            urls = [url]

        add_urls(urls)
        extern_urls = crawl(urls)

        for url in extern_urls:
            # print(f"ajout de l'URL : {url}")
            line.put(url)

        print(f"on a indexé {len(final_urls)} pages, il y en a {line.qsize()} en attente", end='\r')


def main():
    while True:
        if line.qsize() > 10:
            event.set()
            time.sleep(6)



if __name__ == '__main__':
    url = "https://ensai.fr"
    line.put(url)

    try:
        consumers = [Thread(target=consume), Thread(target=consume), Thread(target=consume)]
        for consumer in consumers:
            consumer.start()

        # main()

        # print("\n fine")
        # rows = np.array(set(final_urls))
        # print(len(rows))
        # np.savetxt("crawled_webpages.csv",
        # rows,
        # delimiter =", ",
        # fmt ='% s')

    except KeyboardInterrupt:
        event.set()
        time.sleep(6)
        fh.flush()
        fh.close()




